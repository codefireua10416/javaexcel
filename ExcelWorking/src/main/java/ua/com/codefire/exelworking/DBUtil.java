/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.exelworking;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class DBUtil {

    private String connection;
    private String username;
    private String password;

    public DBUtil(String connection, String username, String password) {
        this.connection = connection;
        this.username = username;
        this.password = password;
    }

    public Connection getConnetion() throws SQLException {
        return DriverManager.getConnection(connection, username, password);
    }

    public List<String> getDatabaseList() throws SQLException {
        List<String> databases = new ArrayList<>();

        try (Connection conn = getConnetion()) {
            ResultSet rs = conn.createStatement().executeQuery("SHOW DATABASES");

            while (rs.next()) {
                databases.add(rs.getString(1));
            }
        }

        return databases;
    }

    public List<String> getTablesList(String database) throws SQLException {
        List<String> tables = new ArrayList<>();

        try (Connection conn = getConnetion()) {
            ResultSet rs = conn.createStatement().executeQuery("SHOW TABLES FROM " + database);

            while (rs.next()) {
                tables.add(rs.getString(1));
            }
        }

        return tables;
    }
}
