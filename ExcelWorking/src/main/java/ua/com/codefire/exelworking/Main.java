/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.exelworking;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        
        String connection = "jdbc:mysql://localhost/test?useSSL=true";
        
        Workbook wb = new XSSFWorkbook();

        Sheet sheet = wb.createSheet();
        
        try (Connection conn = DriverManager.getConnection(connection, "student", "12345")) {
            ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM uploads");
            ResultSetMetaData md = rs.getMetaData();
            
            Row row = sheet.createRow(0);
            
            for (int i = 0; i < md.getColumnCount(); i++) {
                Cell cell = row.createCell(i);
                cell.setCellValue(md.getColumnName(i + 1));
            }
            
            for (int i = 1; rs.next(); i++) {
                row = sheet.createRow(i);
                
                for (int j = 0; j < md.getColumnCount(); j++) {
                    Cell cell = row.createCell(j);
                    cell.setCellValue(rs.getString(j + 1));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        try (FileOutputStream fileOut = new FileOutputStream("workbook.xlsx")) {
            wb.write(fileOut);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
